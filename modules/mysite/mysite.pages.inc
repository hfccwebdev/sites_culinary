<?php

/**
 * @file
 * Contains page callbacks for site module.
 *
 * @see mysite.module
 */

/**
 * Page callback for bake sale item report.
 */
function mysite_bake_sale_items() {
  drupal_set_title(t('Bake Sale Item Totals'));

  $query = db_select('field_collection_item', 'field_collection_item');
  $query->fields('field_collection_item');
  $query->condition('field_name', 'field_order');

  // This join should enforce processing only line items associated with the bake sale.
  // I am not sure it will properly deal with revisions of orders, but this is the best
  // I can do for now.
  $query->join("field_data_field_order", "field_order", "field_collection_item.item_id = field_order.field_order_value AND field_order.entity_type = 'entityform' AND field_order.bundle='hfc_culinary_arts_department_bak' AND field_order.deleted = 0");

  // Get the target ID of the item ordered in this line item.
  $query->join("field_data_field_order_item", "field_order_item", "field_collection_item.item_id = field_order_item.entity_id AND field_collection_item.revision_id = field_order_item.revision_id AND field_order_item.entity_type = 'field_collection_item'");
  $query->fields('field_order_item', array('field_order_item_target_id'));

  // Get the node title for the targeted item.
  $query->join("node", "n", "field_order_item.field_order_item_target_id = n.nid");
  $query->addField('n', 'title', 'title');

  // Get the quantity ordered for this line item.
  $query->join("field_data_field_quantity", "field_quantity", "field_collection_item.item_id = field_quantity.entity_id AND field_collection_item.revision_id = field_quantity.revision_id AND field_quantity.entity_type = 'field_collection_item'");
  $query->addField('field_quantity', 'field_quantity_value', 'quantity');

  $result = $query->execute()->fetchAll();

  $items = array();
  $titles = array();
  foreach ($result as $item) {
    $items[$item->field_order_item_target_id] = array(
      'title' => array(
        'class' => array('title'),
        'data' => check_plain($item->title),
      ),
      'quantity' => array(
        'class' => array('quantity'),
        'data' => !empty($items[$item->field_order_item_target_id]['quantity']['data'])
          ? $items[$item->field_order_item_target_id]['quantity']['data'] + (int) $item->quantity
          : (int) $item->quantity,
      ),
    );
    $titles[$item->field_order_item_target_id] = check_plain($item->title);
  }

  // Sort $items array by $titles values.
  array_multisort($titles, SORT_ASC, $items);

  return array(
    '#theme' => 'table',
    '#header' => array(t('Item'), t('Quantity')),
    '#rows' => $items,
  );
}
