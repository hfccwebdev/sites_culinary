<?php

/**
 * Implements template_preprocess_field().
 */
function culinary_preprocess_field(&$variables) {

  // We want to display the field_order field collection as a table.
  if ($variables['element']['#field_name'] == 'field_order') {
    $rows = array();
    foreach ($variables['items'] as $item) {
      $entity = reset($item['entity']['field_collection_item']);
      $order_item = $entity['field_order_item']['#object']->field_order_item[LANGUAGE_NONE][0]['entity'];
      $rows[] = array(
        array('data' => $entity['field_quantity'][0]['#markup'], 'class' => array('quantity')),
        array('data' => $entity['field_order_item'][0]['#markup'], 'class' => array('order-item')),
        array('data' => number_format($order_item->field_price[LANGUAGE_NONE][0]['value'], 2), 'class' => array('unit-price')),
        array('data' => $entity['mysite_extended_price']['#markup'], 'class' => array('ext-price')),
      );
    }
    $variables['items'] = array(
      array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => array(t('Quantity'), t('Order Item'), t('Price'), t('Ext Price')),
        '#empty' => t('No items to display.'),
      ),
    );
  }
}
